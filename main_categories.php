<?php
session_start();
if (isset($_SESSION["password"]) and isset($_SESSION["email"]) and !empty(["password"]) and !empty($_SESSION["email"])) {
} else {
    header('Location: ' . 'login.php');
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>Panel - Main Categories</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="css/all_css.css"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.11.5/sweetalert2.min.css"/>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.11.5/sweetalert2.min.js"></script>
    <link rel="stylesheet" type="text/css" href="style.css">
    <script src="https://use.fontawesome.com/939e9dd52c.js"></script>

    <style>
        body {
            background: none;
        }

        .mainDiv {
            width: 100% !important;
            margin-top: 0px;
        }
    </style>

    <script src="https://www.gstatic.com/firebasejs/4.6.1/firebase.js"></script>

    <script src="js/session.js"></script>
    <script>
        $.session.set("email", "<?= $_SESSION["email"] ?>");
        $.session.set("password", "<?= $_SESSION["password"] ?>");
    </script>
    <script src="index.js"></script>

</head>
<body>
<?php include '_panel.php' ?>
<div class="container">
    <div class="row"><div class="col-md-7"><h2>Main Categories </h2></div><div class="col-md-5"><h2><a  href="add_category.php" class="btn btn-success pull-right ">Add Main Category</a></h2></div></div>

    <ul class="list-group" id="list_m-categories-div">
        <li>
            <div class="row">
                <div class="col-md-12" style="text-align: center;">
                    <img src="img/loading.gif" style="max-width: 105px;margin-top: 50px;"/>
                </div>
            </div>
        </li>
    </ul>
    <!-- Modal -->
    <div class="modal fade" id="mc-category-modal" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
<!--            <div class="modal-content">-->
                    <div class="mainDiv modal-content" align="right">
                        <div class="modal-body">
                            <h1 id="modal-mc-name" align="left"></h1>
                            <input hidden id="modal-mc_mainIndex" value=""/>

                            <input id="modal-mc_mainName" placeholder="Enter Name here...">
                            <img src="img/loading.gif" id="u-loader" style="display: none;width: 100px;">
                            <button style="cursor: pointer;" onclick="editMCategory()"><i
                                        class="fa fa-arrow-right" aria-hidden="true"></i></button>
                        </div>
                    </div>
<!--            </div>-->

        </div>
    </div>

</div>

</body>
</html>
