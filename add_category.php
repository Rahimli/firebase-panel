<?php
session_start();
if(isset($_SESSION["password"])and isset($_SESSION["email"]) and !empty(["password"]) and !empty($_SESSION["email"])){}else{header('Location: ' . 'login.php');}
?>

<!DOCTYPE html>
<html>
<head>
	<title>Panel - Add Main Category</title>

	<link href="https://fonts.googleapis.com/css?family=Raleway:300,400,500,700" rel="stylesheet">

	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.11.5/sweetalert2.min.css"/>
	<script src="https://use.fontawesome.com/939e9dd52c.js"></script>

	<link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
<div class="js">
    <body>
    <div id="u-loader"></div>
    </body>
</div>
<?php include '_panel.php'?>
	<div class="mainDiv" align="right">
		<!--<p><input  id="mc_mainIndex" placeholder="Enter Index here..."/></p>-->
        <div class="row">
            <div class="col-md-11">
                <h1 align="left">Add Main Category  </h1>
            </div>
            <div class="col-md-1"><h1 align=""><a class="btn btn-danger" href="main_categories.php"><i class="fa fa-times" aria-hidden="true"></i></a></h1></div>
        </div>
		<input id="mc_mainName" placeholder="Enter Name here...">
		<button style="cursor: pointer;" onclick="addNewMCategory()" id="submitButton" ><i class="fa fa-arrow-right" aria-hidden="true"></i></button>
<!--		<a class="goback-button" href="main_categories.php" style="" ><i class="fa fa-arrow-left" aria-hidden="true"></i></a>-->
	</div>

	<script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.2.1.min.js"></script>
<script src="https://www.gstatic.com/firebasejs/4.6.1/firebase.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.11.5/sweetalert2.min.js"></script>
<script src="js/session.js"></script>
<script>
    $.session.set("email", "<?= $_SESSION["email"] ?>");
    $.session.set("password", "<?= $_SESSION["password"] ?>");


    jQuery(document).ready(function($) {
        $('#u-loader').fadeOut();
    });
</script>
<script src="index.js"></script>
	
</body>
</html>