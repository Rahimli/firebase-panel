<?php
session_start();
if(isset($_SESSION["password"])and isset($_SESSION["email"]) and !empty(["password"]) and !empty($_SESSION["email"])){}else{header('Location: ' . 'login.php');}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>Panel - Details</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="list.css">
    <link rel="stylesheet" href="css/all_css.css"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.11.5/sweetalert2.min.css"/>
    <link rel="stylesheet" type="text/css" href="style.css">
    <link rel="stylesheet" href="css/cropper.css"/>
    <link rel="stylesheet" href="css/cropper-admin-main.css"/>
    <script src="https://use.fontawesome.com/939e9dd52c.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.11.5/sweetalert2.min.js"></script>
    <script src="https://www.gstatic.com/firebasejs/4.6.1/firebase.js"></script>
    <script src="js/cropper-custom-main.js"></script>
    <script src="js/cropper.js"></script>
    <script src="js/session.js"></script>
    <script>
        $.session.set("email", "<?= $_SESSION["email"] ?>");
        $.session.set("password", "<?= $_SESSION["password"] ?>");
    </script>
    <script src="index.js"></script>

    <style>
        body {
            background: none;
        }

        .mainDiv {
            width: 100% !important;
            margin-top: 0px;
        }
        .current {
            color: green;
        }
        #pagin li {
            display: inline-block;
        }
    </style>
</head>
<body>

<?php include '_panel.php'?>

<div class="container">
    <h2>Details <a href="add_detail.php" class="btn btn-success  pull-right ">Add Detail</a></h2>

    <ul class="list-group" id="list_m-details-div">
        <li>
            <div class="row">
                <div class="col-md-12" style="text-align: center;">
                    <img src="img/loading.gif" style="max-width: 105px;margin-top: 50px;"/>
                </div>
            </div>
        </li>

    </ul>

    <ul id="pagin">

    </ul>

    <div class="modal fade" id="d-detail-modal" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <!--            <div class="modal-content">-->
            <div class="mainDiv modal-content" align="right">
                <div class="modal-body">
                    <h1 id="modal-td-name" align="left"></h1>
                    <p>
                        <select id="modal-c_Category">
                            <!--<option value="" disabled selected hidden>Select Category</option>-->
                        </select>
                    </p>
                    <!--<p><input  id="c_mainIndex" placeholder="Enter Index here..."/></p>-->
                    <!--<p><input  id="c_mainImage" placeholder="Enter Image here..."/></p>-->
                    <div class="portlet-body">
                        <!-- BEGIN FORM-->
                        <div class="form-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="img-container">
                                        <img id="main-image" src="">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <img id="cropped-main-image" src="" class="img-responsive">
                                </div>
                                <div class="col-md-6">
                                    <img id="result-cropped-main-image" src="img/default.png" class="preview-sm">
                                    <input type="hidden" id="hidden-result-cropped-main-image" name="result_cropped_main_image"
                                           value=""/>
                                    <input type="hidden" id="hidden-original-main-image" value=""/>
                                </div>
                            </div>
                            <div class="row" id="actions">
                                <div class="col-md-9 docs-buttons">
                                    <!-- <h3>Toolbar:</h3> -->
                                    <div class="btn-group">
                                        <a href="javascript:void(0)" class="btn btn-info" data-method="zoom" data-option="0.1"
                                           title="Zoom In">
                                        <span class="docs-tooltip" data-toggle="tooltip" title="cropper.zoom(0.1)">
                                            <span class="fa fa-search-plus"></span>
                                        </span>
                                        </a>
                                        <a href="javascript:void(0)" class="btn btn-info" data-method="zoom" data-option="-0.1"
                                           title="Zoom Out">
                                        <span class="docs-tooltip" data-toggle="tooltip" title="cropper.zoom(-0.1)">
                                            <span class="fa fa-search-minus"></span>
                                        </span>
                                        </a>
                                    </div>
                                    <div class="btn-group">
                                        <a href="javascript:void(0)" class="btn btn-primary" data-method="move" data-option="-10"
                                           data-second-option="0"
                                           title="Move Left">
                                        <span class="docs-tooltip" data-toggle="tooltip" title="cropper.move(-10, 0)">
                                            <span class="fa fa-arrow-left"></span>
                                        </span>
                                        </a>
                                        <a href="javascript:void(0)" class="btn btn-primary" data-method="move" data-option="10"
                                           data-second-option="0"
                                           title="Move Right">
                                        <span class="docs-tooltip" data-toggle="tooltip" title="cropper.move(10, 0)">
                                            <span class="fa fa-arrow-right"></span>
                                        </span>
                                        </a>
                                        <a href="javascript:void(0)" class="btn btn-primary" data-method="move" data-option="0"
                                           data-second-option="-10"
                                           title="Move Up">
                                        <span class="docs-tooltip" data-toggle="tooltip" title="cropper.move(0, -10)">
                                            <span class="fa fa-arrow-up"></span>
                                        </span>
                                        </a>
                                        <a href="javascript:void(0)" class="btn btn-primary" data-method="move" data-option="0"
                                           data-second-option="10"
                                           title="Move Down">
                                        <span class="docs-tooltip" data-toggle="tooltip" title="cropper.move(0, 10)">
                                            <span class="fa fa-arrow-down"></span>
                                        </span>
                                        </a>
                                    </div>

                                    <div class="btn-group">
                                        <label class="btn btn-primary btn-upload" for="inputImage" title="Upload image file">
                                            <input type="file" class="sr-only" id="inputImage" name="file"
                                                   accept=".jpg,.jpeg,.png,.gif,.bmp,.tiff">

                                            <span class="docs-tooltip" data-toggle="tooltip" title="Şəkil Seç">
                                            <span class="fa fa-upload"></span>
                                        </span>
                                        </label>
                                    </div>
                                    <div class="btn-group btn-group-crop">
                                        <a href="javascript:void(0)" class="btn btn-primary" data-method="getCroppedCanvas"
                                           data-option="{ &quot;width&quot;: 480, &quot;height&quot;: 270 }">
                                        <span class="docs-tooltip" data-toggle="tooltip"
                                              title="Crop and Show">
                                            Crop and Show
                                        </span>
                                        </a>
                                    </div>
                                    <!-- Show the cropped image in modal -->
                                    <div class="modal fade docs-cropped" id="getCroppedCanvasModal" role="dialog" aria-hidden="true"
                                         aria-labelledby="getCroppedCanvasTitle" tabindex="-1">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <a href="javascript:void(0)" class="close" data-dismiss="modal" aria-hidden="true">&times;</a>
                                                    <h4 class="modal-title" id="getCroppedCanvasTitle">Cropped</h4>
                                                </div>
                                                <div class="modal-body"></div>
                                                <div class="modal-footer">
                                                    <a href="javascript:void(0)" class="btn btn-default" data-dismiss="modal">Close</a>
                                                    <a href="javascript:void(0)" class="btn btn-primary" id="download"
                                                       href="javascript:void(0);"
                                                       download="cropped.jpg">Download</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div><!-- /.modal -->
                                </div><!-- /.docs-buttons -->
                            </div>
                        </div>
                    </div>

                    <p><input type="hidden" id="modal-de_mainIndex" placeholder=""/></p>
                    <p><input  id="modal-de-name" placeholder="Enter Name here..."/></p>
                    <p><input  id="modal-de-price" placeholder="Enter Price here..."/></p>
                    <p><input  id="modal-de-url" placeholder="Enter Url here..."/></p>
                    <p><textarea  id="modal-de-description" rows="4" placeholder="Enter Description here..."></textarea></p>

                    <img   src="img/loading.gif" id="u-loader" style="display: none;width: 100px;">
                    <button style="cursor: pointer;" onclick="editDetail()">
                        <i class="fa fa-arrow-right" aria-hidden="true"></i>
                    </button>
                </div>
            </div>
            <!--            </div>-->

        </div>
    </div>


</div>


</body>
