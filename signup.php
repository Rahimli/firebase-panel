<?php
/**
 * Created by PhpStorm.
 * User: rahim
 * Date: 11/09/2017
 * Time: 23:20
 */
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Panel - Signup</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.11.5/sweetalert2.min.css"/>


    <script src="https://www.gstatic.com/firebasejs/4.6.1/firebase.js"></script>
    <link rel="stylesheet" href="css/all_css.css"/>
    <script src="js/session.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.11.5/sweetalert2.min.js"></script>
    <script src="index.js"></script>
</head>
<body>


<?php include '_panel.php'?>
<div class="container">
    <div class="row">
        <div class="col-md-4 col-md-offset-4">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Sign up via System</h3>
                </div>
                <div class="panel-body">
<!--                    <form accept-charset="UTF-8" role="form" method="post">-->
                        <fieldset>
                            <div class="form-group">
                                <input class="form-control" placeholder="yourmail@example.com" id="signup-email" name="signup-email" type="text">
                            </div>
                            <div class="form-group">
                                <input class="form-control" placeholder="Password" id="signup-password" name="signup-password" type="password" value="">
                            </div>
                            <div class="form-group">
                                <input class="form-control" placeholder="Repeat password" id="signup-password-repeat" name="signup-password-repeat" type="password" value="">
                            </div>
                                <a onclick="createUser()" class="btn btn-lg btn-success btn-block" >Sign up</a>
                        </fieldset>
<!--                    </form>-->
                    <hr/>
<!--                    <center><h4>OR</h4></center>-->
<!--                        <a href="login.php" class="btn btn-lg btn-info btn-block">Login</a>-->
                </div>
            </div>
        </div>
    </div>
</div>

</body>
</html>


