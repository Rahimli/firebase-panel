// Initialize Firebase
// console.log($.session.get('email'));

if ($.session.get('email') == null && $.session.get('password') == null) {
    window.location.href = "login.php";
}

var config = {
    // apiKey: "AIzaSyBV7A9im7szdZww0GqtL49dwxLB4Hkbr7U",
    // authDomain: "realtimedb-73998.firebaseapp.com",
    // // email:"ataxanr@gmail.com",
    // databaseURL: "https://realtimedb-73998.firebaseio.com",
    // projectId: "realtimedb-73998",
    // storageBucket: "realtimedb-73998.appspot.com",
    // messagingSenderId: "612110204461"



    apiKey: "AIzaSyClXd-UlBCSLZ5VOAqngdSy25FQh-nXsvU",
    authDomain: "larsen-68317.firebaseapp.com",
    databaseURL: "https://larsen-68317.firebaseio.com",
    projectId: "larsen-68317",
    storageBucket: "larsen-68317.appspot.com",
    messagingSenderId: "468692234867"
};

firebase.initializeApp(config);
firebase.auth().signInWithEmailAndPassword($.session.get('email'), $.session.get('password')).catch(function (error) {
    // Handle Errors here.
    var errorCode = error.code;
    var errorMessage = error.message;
    window.location.href = "login.php";
    // ...
});
function c_email_tester(p1) {
    var input = $(p1);
    var re = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
    var bool = re.test(input.val());
    if (bool) {
        input.removeClass("invalid").addClass("valid");
    }
    else {
        input.removeClass("valid").addClass("invalid");
    }
    return bool;
}
function createUser() {
    var s_email = $('#signup-email');
    var s_password = $('#signup-password');
    var s_repeat_password = $('#signup-password-repeat');
    if (!c_email_tester(s_email)) {
        alert('Email incorrect format');
        return false;
    }
    if (s_password.val().trim() < 7) {
        alert('Password must be greater than 7');
        return false;
    }
    if (s_email.val().trim() || s_password.val().trim() || s_repeat_password.val().trim()) {
        if (s_password.val().trim() == s_repeat_password.val().trim()) {
// alert('Salam');
            firebase.auth().createUserWithEmailAndPassword(s_email.val(), s_password.val()).then(function (user) {
                // console.log('everything went fine');
                // console.log('user object:' + user);
                window.location.href = "login.php";
                //you can save the user data here.
            }).catch(function (error) {
                // console.log('there was an error');
                var errorCode = error.code;
                var errorMessage = error.message;
                swal(
                    'Warning!',
                    error.message
                    // 'error'
                );
            });
        } else {
            alert('passwords not same');
        }
    } else {
        alert('fill in both fields');
    }
}
// firebase.initializeApp(config);

var c_mainMCategory = $('#c_mainMcCategory');
var c_mainCategory = $('#c_mainCategory');
var c_Category = $('#c_Category');
//alert("Salam");
// var mc_mainIndex = document.getElementById('mc_mainIndex');
var mc_mainName = document.getElementById('mc_mainName');
// var c_mainIndex = document.getElementById('c_mainIndex');
var c_mainName = document.getElementById('c_mainName');
var c_mainImage = document.getElementById('hidden-result-cropped-main-image');
var d_mainName = document.getElementById('d_mainName');
var d_indexImg = document.getElementById('hidden-result-cropped-main-image');
var d_mainPrice = document.getElementById('d_mainPrice');
var d_mainUrl = document.getElementById('d_mainUrl');
var d_mainDescription = document.getElementById('d_mainDescription');
var u_loader = $('#u-loader');
// var d_object_name = document.getElementById('d_object_name');
var submitButton = $('#submitButton');
var database = firebase.database();

var storageRef = firebase.storage().ref();
var mountainImagesRef = storageRef.child('images/mountains.jpg');


// Check object is empty return true or false
function isEmpty(obj) {
    for (var key in obj) {
        if (obj.hasOwnProperty(key))
            return false;
    }
    return true;
}

// String trim function
function trim(str) {
    return str.replace(/^\s+|\s+$/g, "");
}


var select_m_categories = '';
// var select_categories = '';
// var list_categories = '';
// function listSelectUl() {

    database.ref().child('main_categories').on('value', function (snapshot) {
        // var select_categories = '<option value="" disabled selected hidden>Select Category</option>';
        // var list_categories = '';
        select_m_categories = '<option value="" disabled selected hidden>Select Category</option>';

        // var select_m_categories = select_m_categories + '<option value="" disabled selected hidden>Select Category</option>';
        list_m_categories = '';
        var i = 1;
        $.each(snapshot.val(), function (key, value) {
            select_m_categories = select_m_categories + '<option value="' + value['index'] + '" >' + value['name'] + '</option>';
            list_m_categories = list_m_categories + '<li class="list-group-item"><div class="row"><div class="col-md-9">'+ i + '.&nbsp;&nbsp;&nbsp;&nbsp;' + value['name'] + '</div><div class="col-md-3"><div class="pull-right action-buttons"><a onclick="editModalMCategory(\'' + value['index'] + '\',' + '\'' + value['name'] + '\')" class="btn btn-warning"><span class="glyphicon glyphicon-pencil"></span></a> &nbsp;|&nbsp; <a onclick="removeMCategory(\'' + value['index'] + '\')" class="btn btn-danger"><span class="glyphicon glyphicon-trash"></span></a></div></div></li>';
            i++;
        });
        //
        var mc_count = i-1;
        c_mainMCategory.html(select_m_categories);
        $('#list_m-categories-div').html(list_m_categories);
        $('#index-main-category-count').html('<strong>'+mc_count+'</strong>');
        try {
            getMCategoryUrl();
        } catch (err) {
        }
    });
// }
// listSelectUl();

var select_categories ;
var list_categories ;
database.ref().child('categories').on('value', function (snapshot) {
    list_categories = '';
    select_categories = '<option value="" disabled selected hidden>Select Category</option>';
    var j = 1;
    var mc_count = 1;
    var main_category_name;
    $.each(snapshot.val(), function (key, value) {
        var i = 1;
        main_category_name = '';
        var datadata = database.ref().child('main_categories').child(key).once('value', function (main_categories) {
            list_categories = list_categories + '<li  class="list-group-item main-category"><div class="row"><div class="col-md-9"> '   + i+ '.&nbsp;&nbsp;&nbsp;&nbsp;' + main_categories.val().name + '</div><div class="col-md-3">' + '<a class="btn btn-success pull-right" href="add_pm_category.php?mcategory=' + key + '">+</a></div></div></li>';
            main_category_name = main_categories.val().name;
            mc_count = i;
            i++;
        });
        $.each(value, function (key2, value2) {
            // console.log(value2);
            select_categories = select_categories + '<option value="' + key2 + '">' + main_category_name + ' - ' + value2['name'] + '</option>';
            list_categories = list_categories + '<li class="list-group-item padding-35"><div class="row"><div class="col-md-9">' + j + '.&nbsp;&nbsp;&nbsp;&nbsp;' +  '<img style="width: 60px;" src="' + value2['image'] + '"/> ' + '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + value2['name'] + '</div><div class="col-md-3">' + '<div class="pull-right action-buttons"><a onclick="editModalCategory(\'' + key + '\',' + '\'' + key2 + '\',' + '\'' + value2['name'] + '\',' + '\'' + value2['image'] + '\')" class="btn btn-warning"><span class="glyphicon glyphicon-pencil"></span></a> &nbsp;|&nbsp; <a onclick="removeCategory(\'' + key + '\',' + '\'' + key2 + '\')" class="btn btn-danger"><span class="glyphicon glyphicon-trash"></span></a> </div>' + '</div></div> </li>';
            j++;
        });
    });

    var c_count = j-1;
    c_Category.html(select_categories);
    $('#index-category-count').html('<strong>'+c_count+'</strong>');
    $('#index-category-mc-count').html('<strong>'+mc_count+'</strong>');
    try {
        getCategoryUrl();
    }
    catch (err) {
    }
    if (j > 1) {
        // $('#list_categories-div').empty();
        $('#list_categories-div').html(list_categories);
    }

});




var list_details ;
database.ref().child('detail').on('value', function (snapshot) {
    list_details = '';
    var category_name;
    var category_image;
    var category_index;
    var j = 1;
    var i = 0;
    $.each(snapshot.val(), function (key, value) {
        // j = 1;
        category_name = '';
        category_image = '';
        category_index = '';
        database.ref().child('categories').on('value', function (snapshot2) {
            // i = 0;
            // list_categories = list_categories + '<li  class="list-group-item main-category"><div class="row"><div class="col-md-9"> ' + j + main_categories.val().name + '</div><div class="col-md-3">' + '<a class="btn btn-success pull-right" href="add_pm_category.php?mcategory=' + key + '">+</a></div></div></li>';
            $.each(snapshot2.val(), function (key2, value2) {
                $.each(value2, function (key3, value3) {
                    if(key==key3){
                        // alert('Salam');
                        // alert(key + '-' + key3);
                        category_name = value3['name'];
                        category_image = value3['image'];
                        category_index = value3['index'];
                        i++;
                    }
                });
            });
        });
        list_details = list_details + '<li class="list-group-item main-category"><div class="row"><div class="col-md-10">' + '<img style="width: 60px;" src="' + category_image + '"/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + category_name + '</div><div class="col-md-2">' + '<a class="btn btn-success pull-right" href="add_detail.php?category=' + category_index + '">+</a></div></div></li>';
        var k = 1;
        // });
        var category_parent = false;
        $.each(value, function (key2, value2) {
            // console.log(value2);
            if(category_parent == false) {
                category_parent = true;
            }
            var v_descr;
            var v_url ;
            // try {
                v_descr = value2['description'];
                v_url = value2['url'];
            var extra_div;
            if((isUndefined(v_descr)  || v_descr.trim()=='')&& (isUndefined(v_url) || v_url.trim()=='')){
                extra_div = '';
            }else{
                if((isUndefined(v_descr)  || v_descr.trim()=='') && !(isUndefined(v_url) || v_url.trim()=='')){
                    extra_div = '<div class="row row-detail-extra" ><div class="col-md-9 text-left" >' + 'N/A' + '</div><div class="col-md-3 text-right"><a class="btn btn-info btn-md" target="_blank" href="' + v_url + '"><span class="glyphicon glyphicon-link"></span> Url</a> </div></div>';
                }
                if(!(isUndefined(v_descr)  || v_descr.trim()=='') && (isUndefined(v_url) || v_url.trim()=='')){
                    extra_div = '<div class="row row-detail-extra" ><div class="col-md-9 text-left" >' + v_descr + '</div></div>';
                }else {
                    extra_div = '<div class="row row-detail-extra" ><div class="col-md-9 text-left" >' + v_descr + '</div><div class="col-md-3 text-right"><a class="btn btn-info btn-md" target="_blank" href="' + v_url + '"><span class="glyphicon glyphicon-link"></span> Url</a> </div></div>';
                }
            }
            // }catch (e){
                v_descr = '';
                v_url = '';
            isUndefined(value2['url']) ? v_url = '' : v_url = value2['url'];
            isUndefined(value2['description']) ? v_descr = '' : v_descr = value2['description'];
            list_details = list_details + '<li class="list-group-item padding-35"><div class="row"><div class="col-md-7">' + j + '.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'+' <img style="width: 60px;" src="' + value2['img'] + '"/>' + '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'+ value2['name'] + '</div><div class="col-md-3"> ' + value2['price'] + '</div><div class="col-md-2"> <div class="pull-right action-buttons">&nbsp;&nbsp; <a onclick="editModalDetail(\'' + key + '\',' + '\'' + key2 + '\',' + '\'' + value2['name'] + '\',' + '\'' + value2['price'] + '\',' + '\'' + value2['img'] + '\',' + '\'' + v_url + '\',' + '\'' + v_descr + '\')" class="btn btn-warning"><span class="glyphicon glyphicon-pencil"></span></a> &nbsp;|&nbsp; <a onclick="removeDetail(\'' + key + '\',' + '\'' + key2 + '\')" class="btn btn-danger"><span class="glyphicon glyphicon-trash"></span></a></div></div></div>' +
                extra_div+
                '</li>';
            j++;
            k++;
        });
        if(category_parent==false){
            i--;
        }
    });
    try {
        getCategoryUrl();
    }
    catch (err) {
    }
    if (j > 1) {
        // $('#list_categories-div').empty();
        $('#list_m-details-div').html(list_details);
        var d_count = j-1;
        $('#index-detail-count').html('<strong>'+d_count+'</strong>');
        $('#index-detail-category-count').html('<strong>'+i+'</strong>');
    }
});

function isUndefined(value){
    // Obtain `undefined` value that's
    // guaranteed to not have been re-assigned
    var undefined = void(0);
    return value === undefined;
}
function base64toBlob(base64Data, contentType) {
    contentType = contentType || '';
    var sliceSize = 1024;
    var byteCharacters = atob(base64Data);
    var bytesLength = byteCharacters.length;
    var slicesCount = Math.ceil(bytesLength / sliceSize);
    var byteArrays = new Array(slicesCount);

    for (var sliceIndex = 0; sliceIndex < slicesCount; ++sliceIndex) {
        var begin = sliceIndex * sliceSize;
        var end = Math.min(begin + sliceSize, bytesLength);
        var bytes = new Array(end - begin);
        for (var offset = begin, i = 0; offset < end; ++i, ++offset) {
            bytes[i] = byteCharacters[offset].charCodeAt(0);
        }
        byteArrays[sliceIndex] = new Uint8Array(bytes);
    }
    return new Blob(byteArrays, {type: contentType});
}


function addNewMCategory() {
    // var newMaincategoriesKey = database.ref().child('main_categories').push().key;
    const newMaincategoriesKey = database.ref().child('main_categories').push().key;
    // var pushid=database.ref().push().key;
    // alert(pushid);
    var mcData = {
        index: newMaincategoriesKey,
        name: mc_mainName.value
    };


    u_loader.fadeIn();
    submitButton.hide();
    const mc_objects = database.ref().child('main_categories');
    if (mc_mainName.value.trim() != '') {
        mc_objects.child(newMaincategoriesKey).set(mcData);
        setTimeout(function () {
            window.location.href = "main_categories.php";
        }, 1000);
    }else{
        u_loader.fadeOut();
        submitButton.show();
        swal(
            'Warning!',
            'Please insert correct data'
            // 'error'
        )
    }

    // console.log(database.ref().update(updates));
}

function decodeBase64Image(dataString) {
    var matches = dataString.match(/^data:([A-Za-z-+\/]+);base64,(.+)$/),
        response = {};

    if (matches.length !== 3) {
        return new Error('Invalid input string');
    }

    response.type = matches[1];
    response.data = new Buffer(matches[2], 'base64');

    return response;
}

function dataURItoBlob(dataURI, callback) {
    // convert base64 to raw binary data held in a string
    // doesn't handle URLEncoded DataURIs - see SO answer #6850276 for code that does this
    var byteString = atob(dataURI.split(',')[1]);

    // separate out the mime component
    var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];

    // write the bytes of the string to an ArrayBuffer
    var ab = new ArrayBuffer(byteString.length);
    var ia = new Uint8Array(ab);
    for (var i = 0; i < byteString.length; i++) {
        ia[i] = byteString.charCodeAt(i);
    }

    // write the ArrayBuffer to a blob, and you're done
    var bb = new Blob([ab]);
    return bb;
}

function addNewCategory() {

    var newCategoriesKey = database.ref().child('categories').push().key;

    u_loader.fadeIn();
    submitButton.hide();
// alert(c_mainImage.value);
    // Add new Category
    const mc_object = database.ref().child('categories');
    if (c_mainImage.value.trim() != '' && c_mainName.value.trim() != '' && c_mainMCategory.val() != '') {
        var metadata = {
            contentType: 'image/png'
        };
        $.ajax({
            url: 'base64upload.php',
            data: {base64_image: c_mainImage.value},
            type: 'post',
            complete: function (response) {
                console.log(response);
                var storageRef = firebase.storage().ref();
                storageRef.child(response['responseJSON']['image-link']).put(dataURItoBlob(c_mainImage.value), metadata).then(function (snapshot) {
                    console.log('file3.png Uploaded a blob string!');
                    console.log(snapshot['downloadURL']);
                    var cData = {
                        "image": snapshot['downloadURL'],
                        "index": newCategoriesKey,
                        "name": c_mainName.value
                    };
                    mc_object.child(c_mainMCategory.val()).child(newCategoriesKey).set(cData);
                    window.location.href = "categories.php";
                });
                console.log(dataURItoBlob(c_mainImage.value));
                // console.log(c_mainImage.value.replace("data:image/png;base64,", ""));

            },
            error: function (jqXHR, exception) {
                // alert(jqXHR, exception);
            },
        });
        // decodeBase64Image(c_mainImage.value)
        // firebase.storage().ref('/images').child(newCategoriesKey).putString(base64toBlob(c_mainImage.value, 'image/png'));
        // mc_object.child(c_mainMCategory.val()).child(newCategoriesKey).set(cData);
    }else{
        u_loader.fadeOut();
        submitButton.show();
        swal(
            'Warning!',
            'Please insert correct data'
            // 'error'
        )
    }
}

function addNewDetail() {

    u_loader.fadeIn();
    submitButton.hide();
    var newDetailsKey = database.ref().child('detail').push().key;
    // Add new Category
    const mc_object = database.ref().child('detail');
    if (newDetailsKey != '' && d_mainName.value.trim() != '' && d_mainPrice.value.trim() != '' && c_Category.val().trim() != '') {
        var metadata = {
            contentType: 'image/png'
        };
        $.ajax({
            url: 'base64upload.php',
            data: {base64_image: d_indexImg.value},
            type: 'post',
            complete: function (response) {
                console.log(response);
                var storageRef = firebase.storage().ref();
                storageRef.child(response['responseJSON']['image-link']).put(dataURItoBlob(c_mainImage.value), metadata).then(function (snapshot) {
                    console.log(response['responseJSON']['image-link'] + ' Uploaded a blob string!');
                    console.log(snapshot['downloadURL']);
                    var cData = {
                        "index": newDetailsKey,
                        "name": d_mainName.value,
                        "img": snapshot['downloadURL'],
                        "price": d_mainPrice.value,
                        "description": d_mainDescription.value,
                        "url": d_mainUrl.value
                    };
                    mc_object.child(c_Category.val()).child(newDetailsKey).set(cData);
                    setTimeout(function () {
                        u_loader.fadeOut();
                        window.location.href = "details.php";
                    }, 1500);
                });
                // console.log(dataURItoBlob(c_mainImage.value));
                // console.log(c_mainImage.value.replace("data:image/png;base64,", ""));

            },
            error: function (jqXHR, exception) {
                // alert(jqXHR, exception);
            }
        });

    }else{
        u_loader.fadeOut();;
        submitButton.show();
        swal(
            'Warning!',
            'Please insert correct data'
            // 'error'
        )
    }
}


// **************************************** Edit or Remove functions

// Remove Main Category function
function removeMCategory($key) {
    swal({
        title: 'Are you sure?',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!',
        cancelButtonText: 'No, cancel!',
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',
        buttonsStyling: false
    }).then(function () {
        database.ref().child('main_categories').child($key).remove();
        swal(
            'Deleted!',
            'Main Category has been deleted.',
            'success'
        )
    }, function (dismiss) {});
    // database.ref().child('categories').child($key).remove();
    return true;
}
function editModalMCategory($mc_index, $mc_name) {
    var mc_category_modal = $('#mc-category-modal').modal('show');
    var modal_mc_mainIndex = $('#modal-mc_mainIndex').val($mc_index);
    var modal_mc_mainName = $('#modal-mc_mainName').val($mc_name);
    var modal_mc_name = $('#modal-mc-name').text($mc_name);
}
function editMCategory() {
    var mc_category_modal = $('#mc-category-modal');
    var modal_mc_mainIndex = $('#modal-mc_mainIndex');
    var modal_mc_mainName = $('#modal-mc_mainName');
    var modal_mc_name = $('#modal-mc-name');
    const mc_objects = database.ref().child('main_categories').child(modal_mc_mainIndex.val());
    if (modal_mc_mainName.val().trim() != '' && modal_mc_mainIndex.val().trim() != '') {
        mc_objects.update({name: modal_mc_mainName.val()});
        modal_mc_name.text(modal_mc_mainName.val())
    }
    setTimeout(function(){
        mc_category_modal.modal('hide');
    }, 1000);
}
function editModalCategory($mc_index, $c_index, $c_name, $c_image) {
    $('#c-category-modal').modal('show');
    $('#modal-ca_mainIndex').val($c_index);
    // $('#modal-c_mainName').val($c_name);
    $('#modal-tc-name').text($c_name);
    $('#result-cropped-main-image').attr('src', $c_image);
    $('#modal-ca-name').val($c_name);
    $('#modal-c_mainMcCategory').html(select_m_categories).val($mc_index);
    // $("div.id_100 select").val("val2");
    // alert(select_m_categories);

}
function editCategory() {
    var c_category_modal = $('#c-category-modal');
    var modal_mc_mainIndex = $('#modal-c_mainMcCategory');
    var modal_c_mainIndex = $('#modal-ca_mainIndex');
    var modal_c_name = $('#modal-ca-name');
    var modal_c_image = $('#hidden-result-cropped-main-image');
    // const mc_objects = database.ref().child('main_categories').child(modal_mc_mainIndex.val());


    if (modal_mc_mainIndex.val() != '' && modal_c_mainIndex.val() != '' && modal_c_name.val() != '') {
        // alert('salam 2');
        if (modal_c_image.val().trim() != '') {
            // alert('new salam');
            var metadata = {
                contentType: 'image/png'
            };
            $.ajax({
                url: 'base64upload.php',
                data: {base64_image: modal_c_image.val()},
                type: 'post',
                complete: function (response) {
                    console.log(response);
                    var storageRef = firebase.storage().ref();
                    storageRef.child(response['responseJSON']['image-link']).put(dataURItoBlob(modal_c_image.val()), metadata).then(function (snapshot) {
                        console.log('file3.png Uploaded a blob string!');
                        console.log(snapshot['downloadURL']);
                        var cData = {
                            "name": modal_c_name.val(),
                            "image": snapshot['downloadURL']
                        };
                        var mc_object = database.ref().child('categories').child(modal_mc_mainIndex.val()).child(modal_c_mainIndex.val());
                        mc_object.update(cData);
                        // listSelectUl();
                        c_category_modal.modal('hide');
                    });
                    // console.log(c_mainImage.value.replace("data:image/png;base64,", ""));

                },
                error: function (jqXHR, exception) {
                    // alert(jqXHR, exception);
                },
            });
        } else {
            var cData = {
                "name": modal_c_name.val()
            };
            var mc_object = database.ref().child('categories').child(modal_mc_mainIndex.val()).child(modal_c_mainIndex.val());
            mc_object.update(cData).then(function (data) {
                c_category_modal.modal('hide');
            });
        }
    }
}
// Remove Category function
function removeCategory($key, $index) {
    swal({
        title: 'Are you sure?',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!',
        cancelButtonText: 'No, cancel!',
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',
        buttonsStyling: false
    }).then(function () {
        database.ref().child('categories').child($key).child($index).remove();
        swal(
            'Deleted!',
            'Category has been deleted.',
            'success'
        )
    }, function (dismiss) {});
    return true;
}


function editModalDetail($c_index, $d_index, $d_name, $d_price, $d_image, $d_url, $d_description) {
    $('#d-detail-modal').modal('show');
    $('#modal-de_mainIndex').val($d_index);
    // $('#modal-c_mainName').val($c_name);
    $('#modal-td-name').text($d_name);
    $('#modal-de-price').val($d_price);
    $('#result-cropped-main-image').attr('src', $d_image);
    $('#modal-de-name').val($d_name);
    $('#modal-de-url').val($d_url);
    $('#modal-de-description').val($d_description);
    $('#modal-c_Category').html(select_categories).val($c_index);
    // $("div.id_100 select").val("val2");
    // alert(select_m_categories);

}

// Remove Detail function
function removeDetail($key, $index) {
    swal({
        title: 'Are you sure?',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!',
        cancelButtonText: 'No, cancel!',
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',
        buttonsStyling: false
    }).then(function () {
        database.ref().child('detail').child($key).child($index).remove();
        swal(
            'Deleted!',
            'Detail has been deleted.',
            'success'
        )
    }, function (dismiss) {});
    return true;
}
function editDetail() {
    var detail_modal = $('#d-detail-modal');
    var modal_c_mainIndex = $('#modal-c_Category');
    var modal_d_mainIndex = $('#modal-de_mainIndex');
    var modal_d_name = $('#modal-de-name');
    var modal_d_price = $('#modal-de-price');
    var modal_d_url = $('#modal-de-url');
    var modal_d_description = $('#modal-de-description');
    var modal_d_image = $('#hidden-result-cropped-main-image');
    // const mc_objects = database.ref().child('main_categories').child(modal_mc_mainIndex.val());


    if (modal_c_mainIndex.val().trim() != '' && modal_d_mainIndex.val() != '' && modal_d_name.val() != '' && modal_d_price.val() != '') {
        // alert(modal_d_name.val());
        if (modal_d_image.val().trim() != '') {
            // alert('new salam');
            var metadata = {
                contentType: 'image/png'
            };
            $.ajax({
                url: 'base64upload.php',
                data: {base64_image: modal_d_image.val()},
                type: 'post',
                complete: function (response) {
                    console.log(response);
                    var storageRef = firebase.storage().ref();
                    storageRef.child(response['responseJSON']['image-link']).put(dataURItoBlob(modal_d_image.val()), metadata).then(function (snapshot) {
                        console.log('file3.png Uploaded a blob string!');
                        console.log(snapshot['downloadURL']);
                        var dData = {
                            "name": modal_d_name.val(),
                            "price": modal_d_price.val(),
                            "img": snapshot['downloadURL'],
                            "description": modal_d_description.val(),
                            "url": modal_d_url.val()
                        };
                        var md_object = database.ref().child('detail').child(modal_c_mainIndex.val()).child(modal_d_mainIndex.val());
                        md_object.update(dData);
                        detail_modal.modal('hide');
                    });

                },
                error: function (jqXHR, exception) {
                },
            });
        } else {
            var dData = {
                "name": modal_d_name.val(),
                "price": modal_d_price.val(),
                "description": modal_d_description.val(),
                "url": modal_d_url.val()
            };
            var md_object = database.ref().child('detail').child(modal_c_mainIndex.val()).child(modal_d_mainIndex.val());
            md_object.update(dData);
            detail_modal.modal('hide');
            // window.location.href = "categories.php";
        }
    }
}

