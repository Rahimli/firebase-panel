<?php
session_start();
if (isset($_SESSION["password"]) and isset($_SESSION["email"]) and !empty(["password"]) and !empty($_SESSION["email"])) {
} else {
    header('Location: ' . 'login.php');
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Panel - Home</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="https://www.gstatic.com/firebasejs/4.6.1/firebase.js"></script>
    <script src="https://use.fontawesome.com/939e9dd52c.js"></script>
    <script src="js/session.js"></script>
    <script src="https://www.gstatic.com/firebasejs/4.6.1/firebase.js"></script>

    <script src="js/session.js"></script>
    <script>
        $.session.set("email", "<?= $_SESSION["email"] ?>");
        $.session.set("password", "<?= $_SESSION["password"] ?>");
    </script>
    <script src="index.js"></script>

</head>
<body>
<style>
    .index-information-box {
        margin-top: 11px;
        padding: 0px 15px;
    }

    .index-information-item {
        padding: 7px 2px;
        border-top: 1px solid #f5f5f5;
    }
</style>
<?php include '_panel.php' ?>

<div class="container">
    <div class="row">

        <div class="col-md-4">

            <div class="panel panel-info">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-4">
                            <i class="fa fa-list-ol fa-2x"></i>
                        </div>
                        <div class="col-xs-8 text-right">
                            <!--<p class="announcement-heading">4</p>-->
                            <p class="announcement-text"><strong>Main Categories</strong></p>
                        </div>
                    </div>
                    <!--            <hr>-->
                    <div class="row index-information-box">
                        <div class="col-xs-7 index-information-item">
                            <strong>Main Category Count</strong>
                        </div>
                        <div class="col-xs-5 text-right index-information-item" id="index-main-category-count">
                        </div>
                    </div>
                </div>
                <a href="main_categories.php">
                    <div class="panel-footer announcement-bottom">
                        <div class="row">
                            <div class="col-xs-6">
                                View
                            </div>
                            <div class="col-xs-6 text-right">
                                <i class="fa fa-arrow-circle-right"></i>
                            </div>
                        </div>
                    </div>
                </a>
            </div>

        </div>
        <div class="col-md-4">

            <div class="panel panel-info">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-4">
                            <i class="fa fa-list-alt fa-2x"></i>
                        </div>
                        <div class="col-xs-8 text-right">
                            <!--<p class="announcement-heading">4</p>-->
                            <p class="announcement-text"><strong>Categories</strong></p>
                        </div>
                    </div>
                    <div class="row index-information-box">
                        <div class="col-xs-7 index-information-item">
                            <strong>Category Count</strong>
                        </div>
                        <div class="col-xs-5 text-right index-information-item" id="index-category-count">
                        </div>
                        <div class="col-xs-7 index-information-item">
                            <strong>Main Category Count</strong>
                        </div>
                        <div class="col-xs-5 text-right index-information-item" id="index-category-mc-count">
                        </div>
                    </div>
                </div>
                <a href="categories.php">
                    <div class="panel-footer announcement-bottom">
                        <div class="row">
                            <div class="col-xs-6">
                                View
                            </div>
                            <div class="col-xs-6 text-right">
                                <i class="fa fa-arrow-circle-right"></i>
                            </div>
                        </div>
                    </div>
                </a>
            </div>

        </div>
        <div class="col-md-4">

            <div class="panel panel-info">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-4">
                            <i class="fa fa-home fa-2x"></i>
                        </div>
                        <div class="col-xs-8 text-right">
                            <!--<p class="announcement-heading">4</p>-->
                            <p class="announcement-text"><strong>Details</strong></p>
                        </div>
                    </div>
                    <div class="row index-information-box">
                        <div class="col-xs-7 index-information-item">
                            <strong>Detail Count</strong>
                        </div>
                        <div class="col-xs-5 text-right index-information-item" id="index-detail-count">
                        </div>
                        <div class="col-xs-7 index-information-item">
                            <strong>Category Count</strong>
                        </div>
                        <div class="col-xs-5 text-right index-information-item" id="index-detail-category-count">
                        </div>
                    </div>
                </div>
                <a href="details.php">
                    <div class="panel-footer announcement-bottom">
                        <div class="row">
                            <div class="col-xs-6">
                                View
                            </div>
                            <div class="col-xs-6 text-right">
                                <i class="fa fa-arrow-circle-right"></i>
                            </div>
                        </div>
                    </div>
                </a>
            </div>

        </div>
    </div>

</div>


<script src="https://www.gstatic.com/firebasejs/4.6.1/firebase.js"></script>
<link rel="stylesheet" href="css/all_css.css"/>
<script src="js/session.js"></script>
<script src="index.js"></script>

</body>
</html>
