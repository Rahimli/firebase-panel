<?php
/**
 * Created by PhpStorm.
 * User: rahim
 * Date: 11/09/2017
 * Time: 23:17
 */
session_start();


?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Panel - Login</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="js/session.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.11.5/sweetalert2.min.css"/>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.11.5/sweetalert2.min.js"></script>
    <script src="https://www.gstatic.com/firebasejs/4.6.1/firebase.js"></script>

</head>
<body>
<nav class="navbar navbar-default">
    <div class="container-fluid">
        <div class="navbar-header">
            <a class="navbar-brand" href="index.php">Panel</a>
        </div>
        <ul class="nav navbar-nav">
            <!--<li class="active"><a href="#">Home</a></li>-->
            <li><a href="main_categories.php">Main Categories</a></li>
            <li><a href="categories.php">Categories</a></li>
            <li><a class="active" href="details.php">Details</a></li>
        </ul>
    </div>
</nav>
<div class="container">
    <div class="row">
        <div class="col-md-4 col-md-offset-4">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Login via System</h3>
                </div>
                <div class="panel-body">
                    <form accept-charset="UTF-8" role="form" method="post">
                        <fieldset>
                            <div class="form-group">
                                <input class="form-control" placeholder="yourmail@example.com" name="email" type="text">
                            </div>
                            <div class="form-group">
                                <input class="form-control" placeholder="Password" name="password" type="password" value="">
                            </div>
                            <input class="btn btn-lg btn-success btn-block" name="login" type="submit" value="Login">
                        </fieldset>
                    </form>
                    <hr/>
<!--                    <center><h4>OR</h4></center>-->
<!--                    <a href="signup.php" class="btn btn-lg btn-info btn-block">Sign up</a>-->
                </div>
            </div>
        </div>
    </div>
</div>

</body>
</html>

<?php
$message="";
//session_destroy();
if(!empty($_POST["login"])) {
    if(isset($_POST["email"]) and !empty($_POST["email"]) and isset($_POST["password"]) and !empty($_POST["password"]) ){?>

        <script>
            var sign_in_val=true;
            var config = {

//                 apiKey: "AIzaSyBV7A9im7szdZww0GqtL49dwxLB4Hkbr7U",
//                 authDomain: "realtimedb-73998.firebaseapp.com",
//                 // email:"ataxanr@gmail.com",
//                 databaseURL: "https://realtimedb-73998.firebaseio.com",
//                 projectId: "realtimedb-73998",
//                 storageBucket: "realtimedb-73998.appspot.com",
//                 messagingSenderId: "612110204461"

//
//
                apiKey: "AIzaSyClXd-UlBCSLZ5VOAqngdSy25FQh-nXsvU",
                authDomain: "larsen-68317.firebaseapp.com",
                databaseURL: "https://larsen-68317.firebaseio.com",
                projectId: "larsen-68317",
                storageBucket: "larsen-68317.appspot.com",
                messagingSenderId: "468692234867"
            };

            firebase.initializeApp(config);
            firebase.auth().signInWithEmailAndPassword('<?= $_POST["email"]?>', '<?= $_POST["password"]?>').catch(function (error) {

                var errorCode = error.code;
                var errorMessage = error.message;
            }).then(function(user) {
                if (user) {
                    <?php $_SESSION["email"] = $_POST["email"];
                    $_SESSION["password"] = $_POST["password"]; ?>
                    $.session.set("email", "<?= $_SESSION["email"] ?>");
                    $.session.set("password", "<?= $_SESSION["password"] ?>");
                    window.location.href = "index.php";
                } else {
                    swal(
                        'Warning',
                        'Email or Password is incorrect !',
//                    'error'
                    );
                }
            });

        </script>
<?php }else{?>
<script>
    swal(
        'Warning',
        'Email or Password is empty !',
//                    'error'
    );
</script>
<?php
    }
}
?>
